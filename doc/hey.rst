Main package documentation
==========================

Python documentation can be great, and with the help of Sphinx, you can do
miracles. You're likely to want to write it in ReStructured Text for that
reason.
