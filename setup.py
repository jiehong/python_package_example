#!/usr/bin/env python
""" Python sdist configuration file
"""

from setuptools import setup

WORLDY_DESCRIPTION = """This package is mainly doing nothing,
but it does so well (or it thinks it does).
"""

# You can set the set of dependencies needed for your package:
REQUIRED = ['six>=1.10',
            'nose>=1.3']

setup(
    name="main_package",
    version="1.0",
    install_requires=REQUIRED,
    # Specifying packages to be shipped gives us control
    # tests are not shipped for example
    packages=['main_package',
              'main_package.sub_package1',
              'main_package.sub_package2'],
    # You can ship some data that are not *.py files as well:
    package_data={'main_package': ['package_data/*']},
    author="Author name",
    author_email="guess@goodluck.oops",
    long_description=WORLDY_DESCRIPTION,
    description='Python Package Skeleton Example',
    license="WTFPLv2",
    url="127.0.0.1",
    platforms=['any'],
)
